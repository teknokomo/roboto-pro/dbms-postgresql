# СУБД для бекенда

## Предварительные требования
На хосте должен быть установлен Docker

## Создание отдельной сети Docker для того чтобы контейнеры могли находить друг друга по именам контейнеров.
```
sudo docker network create -d bridge roboto-pro-network
```

## Создание хранилища для хранения динамических файлов БД вне контейнера на случай его сбоя
```
sudo docker volume create roboto-pro-postgresql-data
```

## Создание контейнера с СУБД
Предположим, что папка backup находится в корне файлов сайта
```
export POSTGRES_PASSWORD=$(openssl rand -base64 32 | cut -c1-12)
echo $POSTGRES_PASSWORD
sudo docker run \
  --name roboto-pro-postgresql \
  --volume roboto-pro-postgresql-data:/var/lib/postgresql/data \
  --volume $(pwd)/backup:/backup \
  --env POSTGRES_DB=tehnokom \
  --env POSTGRES_USER=user \
  --env POSTGRES_PASSWORD=$POSTGRES_PASSWORD \
  --network roboto-pro-network \
  --publish 127.0.0.1:5432:5432 \
  --detach \
  postgres:12-bullseye
```
## Вход в контейнер
```
sudo docker exec -it roboto-pro-postgresql bash
```

## Восстановление БД из резервной копии (в контейнере)
```
dropdb tehnokom
psql -h 127.0.0.1 -U user -d tehnokom < /backup/ГГГГ-ММ-ДД_ЧЧ-ММ-СС_roboto.pro_postgresql.sql
```

## Создание резервной копии БД (в контейнере)
```
pg_dump -h 127.0.0.1 -U postgres -d tehnokom > /backup/$(date +"%Y-%m-%d_%H-%M-%S")_roboto.pro_postgresql.sql
```
